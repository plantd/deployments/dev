# Home Stack

Deploy from a compose stack file.

```sh
docker stack deploy --compose-file stack.yml home
```
